## Signature Pad

####Inclusion

Install with `npm install --save ssh://bitbucket.org/compliancescienceexpress/signature-pad.git`

To add to HTML project, include in header 
```html
<script type="module" src="./node_modules/signature-pad/dist/signature-pad/signature-pad.esm.js"></script>
<script nomodule src="./node_modules/signature-pad/dist/signature-pad/signature-pad.js"></script>
```

For framework compatability, see: https://stenciljs.com/docs/overview

#####Usage

Usage: `<smi-signature-pad></smi-signature-pad>`

On click of the "save" button, a "save" event is emitted, including a PNG capture of the signature box.  

```html
<script>

  let sign_pad = document.querySelector('smi-signature-pad')

  sign_pad.addEventListener('save', function(event){
     let signature = event.details //png version of the signature
  })

</script>
```

Controls can be overridden by using the controls slot. Control functionality can be called using `sign_pad.sign()` and `sign_pad.clear()`

```html

<smi-signature-pad>

<div slot="controls">
    <script>
        let sign_pad = document.querySelector('smi-signature-pad')
    </script>
    <button onclick="sign_pad.sign()">SIGN</button>
    <button onclick="sign_pad.clear()">CLEAR</button>
</div>

</smi-signature-pad>
```

There is a modal mode that includes a backdrop that can be used by clicking a "modal=true" to the component.

```<smi-signature-pad modal=true></smi-signature-pad>```

There is also a "close"  slot to replace the close button, and an `activator` slot for the button to open the signing pad when using component in modal mode. 

The "toggle()" method is also exposed to open/close the signature pad as needed. `sign_pad.toggle()`.