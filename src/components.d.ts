/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';


export namespace Components {
  interface SmiSignaturePad {
    'clear': () => Promise<void>;
    'customactiveclass': string;
    'modal': boolean;
    'sign': () => Promise<void>;
    'toggle': () => Promise<void>;
  }
}

declare global {


  interface HTMLSmiSignaturePadElement extends Components.SmiSignaturePad, HTMLStencilElement {}
  var HTMLSmiSignaturePadElement: {
    prototype: HTMLSmiSignaturePadElement;
    new (): HTMLSmiSignaturePadElement;
  };
  interface HTMLElementTagNameMap {
    'smi-signature-pad': HTMLSmiSignaturePadElement;
  }
}

declare namespace LocalJSX {
  interface SmiSignaturePad extends JSXBase.HTMLAttributes<HTMLSmiSignaturePadElement> {
    'customactiveclass'?: string;
    'modal'?: boolean;
    'onSave'?: (event: CustomEvent<any>) => void;
  }

  interface IntrinsicElements {
    'smi-signature-pad': SmiSignaturePad;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements extends LocalJSX.IntrinsicElements {}
  }
}


