import { Component, h, Method, Event, EventEmitter, State, Watch, Prop } from '@stencil/core';
import SignaturePad from 'signature_pad'

@Component({
  tag: 'smi-signature-pad',
  styleUrl: 'signature.css',
  shadow: true
})
export class Reader {

  /**
   * Initialize
   */
  componentDidLoad() {
    this.pad = new SignaturePad(this.canvas)
  }

  private pad: SignaturePad
  private canvas: HTMLCanvasElement

  @Method()
  async clear() {
    this.pad.clear()
  }

  @Prop() modal = false

  @Event() save: EventEmitter

  @Method()
  async sign() {
    this.save.emit(this.pad.toDataURL("image/png"))
  }

  @State() active: boolean = false;

  @Prop() customactiveclass: string;

  @Method()
  async toggle() {
    this.active = !this.active
  }

  @Watch('active')
  activated() {
    this.clear()
  }

  render() {

    let activeClass = 'full'
    if (this.active) {
      if (this.customactiveclass != '') {
        activeClass += " " + this.customactiveclass + " active ";
      } else {
        // activeClass += ' active'
        activeClass += " active";
      }
    }

    if (this.modal){
      return <div>
        <div class="activator" onClick={() => {
          this.toggle()
        }}>
          <slot name="activator">
            <button>Sign</button>
          </slot>
        </div>

        <div class={activeClass}>
          <div class="close" onClick={() => this.toggle()}>
            <slot name="close">
              <button>Close</button>
            </slot>
          </div>
          <div class="container">
            <canvas ref={(el) => {
              this.canvas = el
            }}>
            </canvas>
            <hr></hr>
            <div class="controls">
              <slot name="controls">
                <button onClick={() => {
                  this.clear()
                }}>Clear
                </button>
                <button onClick={() => {
                  this.sign()
                  this.active = false
                }}>Save
                </button>
              </slot>
            </div>
          </div>
        </div>
      </div>
    }

    return <div class="always-open">
        <div class="container">
          <canvas ref={(el) => {
            this.canvas = el
          }}>
          </canvas>
          <div class="controls">
            <slot name="controls">
              <button onClick={() => {
                this.clear()
              }}>Clear
              </button>
              <button onClick={() => {
                this.sign()
                this.active = false
              }}>Save
              </button>
            </slot>
          </div>
      </div>
    </div>

  }
}
