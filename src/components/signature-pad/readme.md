# smi-signature-pad



<!-- Auto Generated Below -->


## Events

| Event  | Description | Type               |
| ------ | ----------- | ------------------ |
| `save` |             | `CustomEvent<any>` |


## Methods

### `clear() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `sign() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `toggle() => Promise<void>`



#### Returns

Type: `Promise<void>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
